These are a few questions we like to ask potential candidates. If you're one of them, please answer every question with a solution (in code).
Write the code as you would in real world. Try think out loud and explain your interviewer, why you decided to do things in a particular way.
At the end, please commit your changes and push them back up.

Happy coding and thanks for being here :)
