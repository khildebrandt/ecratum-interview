# Question 1
#
# Assume you you have two classes like this:
#
# Class Company
#   has_many :people
# end
#
# Class Person < ActiveRecord::Base
#   belongs_to :company
# end
#
# And a controller like this:
#
# ...
# def index
#   @people = Person.all
# end
#
# And a view like this:
#
# @people.each do |person|
# %tr
#   %td= person.name
#   %td= person.company.name
#
# Question: what kind of problem do you see with this code, how do you fix it?

#controller
def index
	@company=Company.all
end

#view
@company.each do |c|
	c.persons.each do |person|
		%td= 
	end		
end
