# Question 3
#
# Assume you have this Class
#
# class Person < ActiveRecord::Base
#   def foo
#     ...
#   end
# end
#
# How do you make sure that method 'foo' is called every time a the object is saved?

def after_perform
	foo
end
